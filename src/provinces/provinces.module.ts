import { Module } from '@nestjs/common';
import { ProvincesController } from './provinces.controller';
import { ProvincesService } from './provinces.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Province } from './entities/province.entitiy';
import { ProvincesConverter } from './utils/provinces.converter';
import { ProvinceCap } from './entities/province.cap.entity';

@Module({
  controllers: [ProvincesController],
  providers: [ProvincesService, ProvincesConverter],
  imports:[TypeOrmModule.forFeature([Province, ProvinceCap])]
})
export class ProvincesModule {}
