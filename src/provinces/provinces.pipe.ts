import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { common } from '../common/utils';

@Injectable()
export class OnlyActivePipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata): any {

    if (value && common.exists(value.onlyActive)) {
      if (value.onlyActive === 'true') {
        value.onlyActive = '1';
      } else if (value.onlyActive === 'false') {
        value.onlyActive = '0';
      } else {
        throw new BadRequestException('onlyActive can be only false or true')
      }
    }
    return value;
  }
}

export class PostalCodePipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata): any {
    const reg = /^\d+$/;
    if (!common.exists(value.postalCode)) {
      throw new BadRequestException('required Postal Code is Missing')
    } else if (
      value.postalCode.length !== 5 ||
      !reg.test(value.postalCode)
    ) {
      throw new BadRequestException('required Postal Code is Invalid')
    }
    return value;
  }


}
