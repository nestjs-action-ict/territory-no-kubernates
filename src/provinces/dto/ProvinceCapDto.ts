import { Province } from '../interfaces/province.interface';

export class ProvinceCapDto implements Province {

  active: boolean;
  code: string;
  description: string;
  istatCode: string;
  istatProvincialCapital: string;
  regionCode: string;
  cap:string;
  multicap:string;
  targa:string;

}
