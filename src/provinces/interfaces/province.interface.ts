export interface Province {
  code: string;
  description: string;
  istatCode: string;
  istatProvincialCapital: string;
  regionCode: string;
  active: boolean
}
