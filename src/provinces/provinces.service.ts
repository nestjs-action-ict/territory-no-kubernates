import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { Province } from './interfaces/province.interface';
import { Province as ProvinceEntity} from './entities/province.entitiy'
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProvincesConverter } from './utils/provinces.converter';
import { ProvinceCap } from './entities/province.cap.entity';
import { ProvinceCapDto } from './dto/ProvinceCapDto';

@Injectable()
export class ProvincesService {

  constructor(
    @InjectRepository(ProvinceEntity)
    private provinceRepository: Repository<ProvinceEntity>,
    private provincesConverter: ProvincesConverter
  ) {}

  async findAll(): Promise<Province[]> {
    return this.provincesConverter.fromEntities(await this.provinceRepository.find());
  }

  async findAllWihParams(onlyActive: boolean): Promise<Province[]> {
    console.log('FindAll: ', onlyActive);
    return this.provincesConverter.fromEntities(await this.provinceRepository.find({where: {IPR_ATTIVO: onlyActive}}));
  }

  async findByPostalCode(postalCode: string): Promise<Province> {
    try {
      return this.provincesConverter.fromEntity(await this.provinceRepository
        .createQueryBuilder('province')
        .innerJoin(ProvinceCap, 'cap', 'cap.IPC_TARGA = province.IPR_TARGA')
        .where('cap.IPC_CAP=' + postalCode)
        .getOne()
      );
    }
    catch (e) {
      console.log('Exception', e);
      throw new NotFoundException('Invalid Postal Code', 'Not Found')

    }
  }

  async insertProvinceAndCap(provinceCapDto: ProvinceCapDto):Promise<void>{
      await  this.provinceRepository.insert(this.provincesConverter.toProvinceEntity(provinceCapDto));
  }

    async deleteByPostalCode(postalCode: string): Promise<void> {
      try {
        const prov = await this.provinceRepository
          .createQueryBuilder('province')
          .innerJoin(ProvinceCap, 'cap', 'cap.IPC_TARGA = province.IPR_TARGA')
          .where('cap.IPC_CAP=' + postalCode)
          .getOne();

        await this.provinceRepository.remove(prov);
      }
      catch (e) {
        console.log('Exception', e);
        throw new NotFoundException('Invalid Postal Code', 'Not Found') 
      }

    }
}
