import { Injectable } from '@nestjs/common';
import { Province } from '../interfaces/province.interface';
import { ProvinceDto} from '../dto/ProvinceDto';
import {ProvinceCapDto} from '../dto/ProvinceCapDto';
import { Province as ProvinceEntity } from '../entities/province.entitiy';

@Injectable()
export class ProvincesConverter {

  fromEntity(province: ProvinceEntity): Province {
    const result = new ProvinceDto();
    result.code = province.IPR_PROVINCIA;
    result.description = province.IPR_DESCR;
    result.istatCode = province.IPR_ISTAT;
    result.istatProvincialCapital = province.IPR_ISTAT_CAPOL;
    result.regionCode = province.IPR_REGIONE;
    result.active = province.IPR_ATTIVO === '1';
    return result;
  }

  fromEntities(provinces: ProvinceEntity[]): Province[] {
    const result = [];
    provinces.forEach( province => result.push(this.fromEntity(province)));
    return result;
  }

  toProvinceEntity(provinceCapDto: ProvinceCapDto): ProvinceEntity{
    const result = new ProvinceEntity();
    result.IPR_PROVINCIA = provinceCapDto.code;
    result.IPR_DESCR = provinceCapDto.description;
    result.IPR_ISTAT = provinceCapDto.istatCode;
    result.IPR_ISTAT_CAPOL = provinceCapDto.istatProvincialCapital;
    result.IPR_REGIONE = provinceCapDto.regionCode;
    result.IPR_ATTIVO = provinceCapDto.active? '1':'0';
    result.IPR_CAP = provinceCapDto.cap;
    result.IPR_TARGA = provinceCapDto.targa;
    return result;

  }
}


