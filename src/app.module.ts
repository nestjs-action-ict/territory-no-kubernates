import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Province } from './provinces/entities/province.entitiy';
import { ProvincesModule } from './provinces/provinces.module';
import { ProvinceCap } from './provinces/entities/province.cap.entity';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { DealerService } from './dealer/dealer.service';
import { DealerModule } from './dealer/dealer.module';
import {HttpAdapterHost, NestFactory} from "@nestjs/core";
import {GlobalExceptionFilter} from "./filters/global.exception.filter";
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: 'development.env', isGlobal:true}),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'oracle',
        host: configService.get('DB_HOST'),
        port: configService.get<number>('DB_PORT'),
        username: configService.get('DB_USERNAME'),
        password: configService.get('DB_PASSWORD'),
        sid: configService.get('DB_SID'),
        entities: [Province, ProvinceCap],
        synchronize: true,
      }),
      inject: [ConfigService]
    }), 
    ProvincesModule, AuthModule, UsersModule, DealerModule],
  providers: [DealerService]
})
export class AppModule {}

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule);
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new GlobalExceptionFilter())
  await app.listen(8080);
}

bootstrap();




