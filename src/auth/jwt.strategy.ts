import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { jwtConstants } from './constants';
import { DealerConverter } from '../dealer/utils/dealer.converter';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private dealerConverter: DealerConverter) {

    super({
      jwtFromRequest: ExtractJwt.fromHeader(jwtConstants.token_key),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret
    })
  }

  async validate(payload: any) {
   if (payload.user_name && payload.tvei)
    return this.dealerConverter.fromToken(payload)
  }

}
