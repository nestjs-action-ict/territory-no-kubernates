import { Dealer } from '../interfaces/dealer.interface';

export class DealerDto implements Dealer {
  channel: string;
  principalName: string;
  grantedRoles: Array<string>;
  tvei: string;
  market: string;
  group: string;
  union: string;
  chain: string;
}
