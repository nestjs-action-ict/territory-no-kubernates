import { Injectable } from '@nestjs/common';
import { Dealer } from '../interfaces/dealer.interface';
import { DealerDto } from '../dto/dealer.dto';

@Injectable()
export class DealerConverter {

  fromToken(token: any): Dealer {
    const result = new DealerDto();

    result.channel = token.channel;
    result.principalName = token.user_name;
    result.tvei = token.tvei;
    result.market = token.market;
    result.grantedRoles = token.authorities;

    return result;
  }
}
